const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

function validate(email) {
  if (email === null || email === undefined) {
    return false;
  }

  const emailParts = email.split('@');
  if (emailParts.length !== 2 || emailParts[1] === '') {
    return false;
  }

  const emailEnding = emailParts[1];
  return VALID_EMAIL_ENDINGS.includes(emailEnding);
}

async function validateAsync(email) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(validate(email));
    }, 0);
  });
}


function validateWithThrow(email) {
  if (!validate(email)) {
    throw new Error(`Invalid email: ${email}`);
  }
  return true;
}

function validateWithLog(email) {
  const result = validate(email);
  console.log(`Email: ${email}, Valid: ${result}`);
  return result;
}

export { validate, validateAsync, validateWithThrow, validateWithLog };
